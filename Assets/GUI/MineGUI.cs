﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineGUI : MonoBehaviour
{
    private string textFieldTemp = "20";

    // Automize sizing / positioning would be nice
    // Make Group, position group.
    // Items in group follow the position of the parent.
    // Items can be offset relative to the groups position.
    private void OnGUI()
    {
        if (WsClient.ws.IsConnected)
        {
            GUI.Box(new Rect(10, 10, 100, 90), "Mine Settings");

            textFieldTemp = GUI.TextField(new Rect(20, 40, 80, 20), textFieldTemp);

            if (GUI.Button(new Rect(20, 70, 80, 20), "Execute"))
            {
                if (int.TryParse(textFieldTemp, out int value))
                {
                    Mine.MineForward(value);
                }
            }
        }
        else
        {
            GUI.Box(new Rect(10, 10, 110, 20), "Connection Error");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            WsClient.ws.Send("{\"command\": \"forward\" }");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            WsClient.ws.Send("{\"command\": \"back\" }");
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            WsClient.ws.Send("{\"command\": \"turnLeft\" }");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            WsClient.ws.Send("{\"command\": \"turnRight\" }");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            WsClient.ws.Send("{\"command\": \"up\" }");
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            WsClient.ws.Send("{\"command\": \"down\" }");
        }

        if (Input.GetMouseButtonDown(0))
        {
            WsClient.ws.Send("{\"command\": \"dig\" }");
        }
    }
}
