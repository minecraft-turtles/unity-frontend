﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebSocketSharp;

public class WsClient : MonoBehaviour
{
    public static WebSocket ws;

    private void Start()
    {
        ws = new WebSocket("ws://localhost:9001");

        ws.ConnectAsync();

        ws.OnMessage += (sender, e) =>
        {
            Debug.Log("Message Received from " + ((WebSocket)sender).Url + ", Data : " + e.Data);
        };
    }

    private void Update()
    {
        if (ws == null)
        {
            return;
        }
    }
}