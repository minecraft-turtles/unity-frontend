﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic commands for the turtle
/// </summary>
public class Turtle
{
    // Moving / Rotating

    /// <summary>
    /// Moves turtle forward
    /// </summary>
    /// <param name="times"></param>
    public static void Forward(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"forward\" }");
        }
    }

    public static void Back(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"back\" }");
        }
    }

    public static void TurnLeft(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"turnLeft\" }");
        }
    }

    public static void TurnRight(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"turnRight\" }");
        }
    }

    public static void Up(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"up\" }");
        }
    }

    public static void down(int times = 1)
    {
        for (int i = 0; i < times; i++)
        {
            WsClient.ws.Send("{\"commands\": \"down\" }");
        }
    }

    // Digging
    public static void Dig()
    {
        WsClient.ws.Send("{\"commands\": \"dig\" }");
    }

    public static void DigUp()
    {
        WsClient.ws.Send("{\"commands\": \"digUp\" }");
    }

    public static void DigDown()
    {
        WsClient.ws.Send("{\"commands\": \"digDown\" }");
    }

    // Refuel
    public static string Refuel { get; } = "refuel()";
    public static string GetFuelLevel { get; } = "getFuelLevel()";
}
