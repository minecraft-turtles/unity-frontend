﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine
{
    /// <summary>
    /// Basic mine forward program
    /// </summary>
    /// <param name="x"></param>
    public static void MineForward(int x = 1)
    {
        for (int i = 0; i < x; i++)
        {
            Turtle.Dig();
            Turtle.Forward();
            Turtle.DigUp();
        }
    }
}
